//
//  JSONModel.swift
//  WeatherTest
//
//  Created by Владислав on 6/5/21.
//

import Foundation

struct JSONModel: Decodable {
    let cod: String?
    let message: Int?
    let cnt: Int?
    let list: [DataModel]
    
}
struct DataModel: Decodable {
    let dt: Date
    let main: Main
    let weather: [Weather]
    let clouds: Clouds
    let wind: Wind
    let visibility: Int
    let pop: Double?
    
}

struct Main: Decodable {
    let temp: Double
    let feelsLike: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Int?
    let seaLevel: Int?
    let grndLevel: Int?
    let humidity: Int?
    let tempkf: Int?
    
}

struct Weather: Decodable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Clouds: Decodable {
    let all: Int?
}

struct Wind: Decodable {
    let speed: Double
    let deg: Int
    let gust: Double
}

