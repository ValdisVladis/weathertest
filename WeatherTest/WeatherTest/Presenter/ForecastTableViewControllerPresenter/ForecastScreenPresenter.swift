//
//  SecondScreenPresenter.swift
//  WeatherTest
//
//  Created by Владислав on 6/12/21.
//
import UIKit
import Foundation

protocol ForecastTabViewProtocol: class {
    func update()
    
}

protocol ForecastTabPresenterProtocol: class {
    init(networkService: DataFetcherService)
}

class ForecastTabPresenter: ForecastTabPresenterProtocol {
    let networkService: DataFetcherService!
    var forecastData: [DataModel] = []
    weak var forecastViewDelegate: ForecastTabViewProtocol?
    var locationNameHandler: ((String) -> Void)?
    
    required init(networkService: DataFetcherService) {
        self.networkService = networkService
        getWeatherData()
    }
    
    func setTabViewDelegate(with delegate: ForecastTabViewProtocol) {
        self.forecastViewDelegate = delegate
    }
    
    func getWeatherData() {
        LocationManager.shared.getLocation { [weak self] (location) in
            guard let self = self else {return}
            LocationManager.shared.resolveLocationName(with: location) { [weak self] (locationName) in
                self?.locationNameHandler?(locationName ?? String())
                guard let self = self else {return}
                let urlForDataFetching = "\(URLConstants.baseURL)/data/2.5/forecast?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&units=metric&appid=\(URLConstants.apiKey)"
                self.networkService.requestData(urlString: urlForDataFetching) { [weak self ] (jsonModel) in
                    if jsonModel != nil {
                        self?.forecastViewDelegate?.update()
                    }
                    guard let self = self else { return }
                    self.forecastData = jsonModel?.list ?? []
                }
            }
        }
        
    }
}
