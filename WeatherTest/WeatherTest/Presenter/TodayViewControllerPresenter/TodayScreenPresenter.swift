//
//  Presenter.swift
//  WeatherTest
//
//  Created by Владислав on 6/10/21.
//
import UIKit
import Foundation

protocol WeatherViewProtocol: NSObjectProtocol {
    func displayWeatherInformation(with object: JSONModel)
    
}

protocol WeatherViewPresenterProtocol: class {
    init(view: WeatherViewProtocol, model: JSONModel)
    func displayWeatherOnView(with url: String)
}

class WeatherPresenter {
    
    private let dataFetcherService: DataFetcherService
    weak private var weatherViewDelegate: WeatherViewProtocol?
    var weatherDataHandler: ((JSONModel) -> Void)?
    var locationNameHandler: ((String) -> Void)?
    
    init(with dataFetcherService: DataFetcherService) {
        self.dataFetcherService = dataFetcherService
    }
    
    func setWeatherViewDelegate(weatherViewDelegate: WeatherViewProtocol) {
        self.weatherViewDelegate = weatherViewDelegate
        
    }
    func fetchWeatherData() {
        LocationManager.shared.getLocation { [weak self ] (location) in
            guard let self = self else {return}
            LocationManager.shared.resolveLocationName(with: location) { [weak self ] (locationName) in
                guard let self = self else {return}
                self.locationNameHandler?(locationName ?? String())
            }
            let urlForDataFetching = "\(URLConstants.baseURL)/data/2.5/forecast?lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&units=metric&appid=\(URLConstants.apiKey)"
            self.dataFetcherService.requestData(urlString: urlForDataFetching) { (jsonModel) in
                
                if let  jsonModel = jsonModel {
                    self.weatherDataHandler?(jsonModel)
                    self.weatherViewDelegate?.displayWeatherInformation(with: jsonModel)
                }
            }
        }
        
    }
    
}

