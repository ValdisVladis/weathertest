//
//  LocationManager.swift
//  WeatherTest
//
//  Created by Владислав on 6/5/21.
//

import Foundation
import UIKit
import CoreLocation

class LocationManager: NSObject,  CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    let locationManager = CLLocationManager()
    var completion: ((CLLocation) -> Void)?
    
    public func getLocation(completion: @escaping ((CLLocation) -> Void)) {
        self.completion = completion
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    public func resolveLocationName(with location: CLLocation,
                                    completion: @escaping((String?) -> Void)) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, preferredLocale: .current) { (placemarks, error) in
            guard let place = placemarks?.first, error == nil else {
                completion(nil)
                return
            }
            
            var name = ""
            if let locality = place.locality {
                name += locality
            }
            
            if let administrative = place.administrativeArea {
                name += ", \(administrative)"
            }
            completion(name)
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        completion?(location)
        manager.stopUpdatingLocation()
        
    }
}
