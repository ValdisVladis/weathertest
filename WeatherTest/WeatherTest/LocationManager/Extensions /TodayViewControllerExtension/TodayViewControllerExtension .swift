//
//  TodayViewControllerExtension .swift
//  WeatherTest
//
//  Created by Владислав on 6/15/21.
//

import Foundation
import UIKit

extension TodayViewController: WeatherViewProtocol {
    func displayWeatherInformation(with object: JSONModel) {
        for element in object.list {
            windSpeedLabel.text = "\(Int(element.wind.speed)) km/h"
            pressureLabel.text = "\(Int(element.main.pressure ?? Int())) hPa"
            temperatureLabel.text = "\(Int(element.main.temp)) °C"
            rainLabel.text = "\(Int(element.main.seaLevel ?? Int())) %"
            for item in element.weather {
                weatherDescriptionLabel.text = item.description.capitalized
                weatherIcon.image = getWeatherImage(name: item.icon)
            }
            
        }
        
    }
}
extension TodayViewController {
    func setupUI() {
        shareButton = UIButton.configureUIButton(title: "Share", textColor: .black, state: .normal, event: .touchUpInside, buttonType: .roundedRect, target: self, selector: #selector(shareWeatherWithDifferentApplications))
        temperatureLabel = UILabel.createReusableLabel(font: UIFont(name: "Helvetica", size: 40) ?? UIFont(), textColor: .black, alignment: .left)
        weatherDescriptionLabel = UILabel.createReusableLabel(font: UIFont(name: "Helvetica", size: 40) ?? UIFont(), textColor: .black, alignment: .center)
        timezoneLabel = UILabel.createReusableLabel(font: UIFont(name: "Helvetica", size: 25) ?? UIFont(), textColor: .black, alignment: .center)
        pressureIcon = UIImageView.createReusableImageView(named: "pressure")
        windIcon = UIImageView.createReusableImageView(named: "wind")
        rainIcon = UIImageView.createReusableImageView(named: "rain")
        secondSeparatorView = UIView.createReusableView(viewColor: .gray)
        thirdSeparatorView = UIView.createReusableView(viewColor: .gray)
    }
    
    func setupConstraints() {
        thirdSeparatorView.activateConstraintsOnView(top: nil,
                                                     leading: view.leadingAnchor,
                                                     bottom: shareButton.topAnchor,
                                                     trailing: view.trailingAnchor,
                                                     padding: .init(top: 0, left: 70, bottom: -10, right: -70),
                                                     size: .init(width: 0, height: 1))
        
        secondSeparatorView.activateConstraintsOnView(top: weatherDescriptionLabel.bottomAnchor,
                                                      leading: view.leadingAnchor,
                                                      bottom:nil,
                                                      trailing: view.trailingAnchor,
                                                      padding: .init(top: 50, left: 70, bottom: 0, right: -70),
                                                      size: .init(width: 0, height: 1))
        
        timezoneLabel.activateConstraintsOnView(top: weatherIcon.bottomAnchor,
                                                leading: view.leadingAnchor,
                                                bottom: nil,
                                                trailing: view.trailingAnchor,
                                                padding: .init(top: 5, left: 50, bottom: 0, right: -50),
                                                size: .init(width: 0, height: 40))
        
        shareButton.activateConstraintsOnView(top: nil,
                                              leading: view.leadingAnchor,
                                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                              trailing: view.trailingAnchor,
                                              padding: .init(top: 0, left: 50, bottom: -20, right: -50),
                                              size: .init(width: 0, height: 40))
        
        weatherIcon.activateConstraintsOnView(top: view.safeAreaLayoutGuide.topAnchor,
                                              leading: view.leadingAnchor,
                                              bottom: nil,
                                              trailing: view.trailingAnchor,
                                              padding: .init(top: 50, left: 150, bottom: 0, right: -150),
                                              size: .init(width: 0, height: 100))
        
        temperatureLabel.activateConstraintsOnView(top: timezoneLabel.bottomAnchor,
                                                   leading: view.leadingAnchor,
                                                   bottom: nil,
                                                   trailing: nil,
                                                   padding: .init(top: 10, left: 50, bottom: 0, right: 0),
                                                   size: .init(width: 120, height: 42))
        
        weatherDescriptionLabel.activateConstraintsOnView(top: timezoneLabel.bottomAnchor,
                                                          leading: nil,
                                                          bottom: nil,
                                                          trailing: view.trailingAnchor,
                                                          padding: .init(top: 10, left: 0, bottom: 0, right: -30),
                                                          size: .init(width: 200, height: 42))
        
        pressureIcon.activateConstraintsOnView(top: secondSeparatorView.bottomAnchor,
                                               leading: view.leadingAnchor,
                                               bottom: pressureLabel.topAnchor,
                                               trailing: nil,
                                               padding: .init(top: 50, left: 20, bottom: -10, right: 0),
                                               size: .init(width: 60, height: 40))
        
        pressureLabel.activateConstraintsOnView(top: pressureIcon.bottomAnchor,
                                                leading: view.leadingAnchor,
                                                bottom: nil,
                                                trailing: nil,
                                                padding: .init(top: 10, left: 20, bottom: 0, right: 0),
                                                size: .init(width: 70, height: 50))
        
        windSpeedLabel.activateConstraintsOnView(top: windIcon.bottomAnchor,
                                                 leading: pressureLabel.trailingAnchor,
                                                 bottom: nil,
                                                 trailing: rainLabel.leadingAnchor,
                                                 padding: .init(top: 10, left: 60, bottom: 0, right: -60),
                                                 size: .init(width: 0, height: 42))
        
        windIcon.activateConstraintsOnView(top:secondSeparatorView.bottomAnchor,
                                           leading: pressureIcon.trailingAnchor ,
                                           bottom: nil,
                                           trailing: rainIcon.leadingAnchor,
                                           padding: .init(top: 30, left: 100, bottom: 0, right: -100),
                                           size: .init(width: 40, height: 70))
        
        rainIcon.activateConstraintsOnView(top: secondSeparatorView.bottomAnchor,
                                           leading: nil,
                                           bottom: rainLabel.topAnchor,
                                           trailing: view.trailingAnchor,
                                           padding: .init(top: 50, left: 0, bottom: -10, right: -20),
                                           size: .init(width: 70, height: 50))
        
        rainLabel.activateConstraintsOnView(top: rainIcon.bottomAnchor,
                                            leading: nil,
                                            bottom: nil,
                                            trailing: view.trailingAnchor,
                                            padding: .init(top: 10, left: 0, bottom: 0, right: -20),
                                            size: .init(width: 70, height: 50))
        
    }
}
