//
//  UIButtonExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/7/21.
//

import Foundation
import UIKit

extension UIButton {
    static func configureUIButton(title: String,
                                  textColor: UIColor,
                                  state: UIControl.State,
                                  event: UIControl.Event,
                                  buttonType: ButtonType,
                                  target: Any,
                                  selector: Selector) -> UIButton {
        
        let button = UIButton.init(type: buttonType)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(title, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        button.addTarget(target, action: selector, for: event)
        return button
    }
}
