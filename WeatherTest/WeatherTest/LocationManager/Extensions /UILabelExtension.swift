//
//  UILabelExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/14/21.
//

import Foundation
import UIKit

extension UILabel {
    static func createReusableLabel(font: UIFont, textColor: UIColor, alignment: NSTextAlignment) -> UILabel {
        let reusableLabel = UILabel()
        reusableLabel.font = font
        reusableLabel.textColor = textColor
        reusableLabel.textAlignment = alignment
        reusableLabel.translatesAutoresizingMaskIntoConstraints = false
        return reusableLabel
    }
}
