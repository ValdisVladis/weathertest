//
//  AlertControllerExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//
import UIKit
import Foundation

extension UIAlertController {
    
    static func showNetworkUnavailability(on controller: UIViewController?,
                                          title: String,
                                          message: String,
                                          preferredStyle: UIAlertController.Style) {
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: preferredStyle)
        alertController.addAction(UIAlertAction(title: "Settings",
                                                style: .default,
                                                handler: { (_) in
                                                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                                                              options: [:],
                                                                              completionHandler: nil)
                                                }))
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: .destructive,
                                                handler: nil))
        
        DispatchQueue.main.async {
            controller?.present(alertController, animated: true, completion: nil)
        }
    }
}

