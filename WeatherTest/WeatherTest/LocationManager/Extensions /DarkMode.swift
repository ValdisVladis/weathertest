//
//  DarkMode.swift
//  WeatherTest
//
//  Created by Владислав on 6/5/21.
//
import UIKit
import Foundation

public extension UIApplication {

     func override(_ userInterfaceStyle: UIUserInterfaceStyle) {
         if supportsMultipleScenes {
             for connectedScene in connectedScenes {
                 if let scene = connectedScene as? UIWindowScene {
                     for window in scene.windows {
                          window.overrideUserInterfaceStyle = userInterfaceStyle
                     }
                 }
             }
         }
         else {
             for window in windows {
                 window.overrideUserInterfaceStyle = userInterfaceStyle
             }
         }
     }
 }
