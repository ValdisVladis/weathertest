//
//  UIImageViewExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/15/21.
//

import Foundation
import UIKit

extension UIImageView {
    static func createReusableImageView(named: String) -> UIImageView {
        let reusableImageView = UIImageView()
        reusableImageView.image = UIImage(named: named)
        reusableImageView.translatesAutoresizingMaskIntoConstraints = false
        return reusableImageView
    }
}
