//
//  TabBarControllerExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/3/21.
//

import Foundation
import UIKit

extension UITabBarController {
    func createNavigationController(vc: UIViewController,
                                    selectedStateImage: UIImage,
                                    unselectedStateImage: UIImage)
    -> UINavigationController {
        let viewController = vc
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.tabBarItem.image = unselectedStateImage
        navigationController.tabBarItem.selectedImage = selectedStateImage
        return navigationController
    }
}

