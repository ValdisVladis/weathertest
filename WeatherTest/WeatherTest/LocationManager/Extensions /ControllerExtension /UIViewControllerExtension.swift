//
//  UIViewControllerExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/6/21.
//

import Foundation
import UIKit

fileprivate var containerViewForSpinner: UIView?

extension UIViewController {
    
    func showActivityIndicator() {
        containerViewForSpinner = UIView(frame: self.view.bounds)
        containerViewForSpinner?.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.center = containerViewForSpinner?.center ?? CGPoint()
        activityIndicator.startAnimating()
        containerViewForSpinner?.addSubview(activityIndicator)
        self.view.addSubview(containerViewForSpinner ?? UIView())
    }
    
    func removeActivityIndicator() {
        containerViewForSpinner?.removeFromSuperview()
        containerViewForSpinner = nil
    }
}
