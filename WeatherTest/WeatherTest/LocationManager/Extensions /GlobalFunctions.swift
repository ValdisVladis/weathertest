//
//  GlobalFunctions.swift
//  WeatherTest
//
//  Created by Владислав on 6/4/21.
//

import Foundation
import UIKit

public func getWeatherImage(name: String) -> UIImage? {
    return UIImage(named: name) ?? UIImage()
}
