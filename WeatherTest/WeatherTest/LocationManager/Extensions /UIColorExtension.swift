//
//  UIColorExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/6/21.
//

import Foundation
import UIKit

extension UIColor {
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
