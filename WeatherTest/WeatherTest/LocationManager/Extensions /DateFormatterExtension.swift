//
//  DateFormatterExtension.swift
//  WeatherTest
//
//  Created by Владислав on 6/4/21.
//

import Foundation
import UIKit

extension Date {
    func getFormattedDate(format: String, value: Int?) -> String {
        let date = Calendar.current.date(byAdding: .day, value: value ?? Int(), to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date ?? Date())
        
    }
    
    func getFormattedTime(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
