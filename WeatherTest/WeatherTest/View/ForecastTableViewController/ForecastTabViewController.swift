//
//  ForecastTabViewController.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//

import UIKit

class ForecastTabViewController: UIViewController {
    //    MARK: - Constants
    
    // MARK: - Variables
    private var forecastPresenter = ForecastTabPresenter(networkService: DataFetcherService())
    private var heightForRow: CGFloat = 100
    private var heightForHeader: CGFloat = 20
    
    
    //   MARK: - UIComponents
    let forecastTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.grouped)
        tableView.register(ForecastTableViewCell.self, forCellReuseIdentifier: ForecastTableViewCell.cellIdentifier)
        return tableView
    }()
    
    
    // MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        forecastPresenter.getWeatherData()
        forecastPresenter.setTabViewDelegate(with: self)
        forecastTableView.delegate = self
        forecastTableView.dataSource = self
        forecastPresenter.locationNameHandler = { [weak self] title in
            guard let self = self else {return}
            self.title = title
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
    }
    //MARK: - AutoLayout
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.addSubview(forecastTableView)
        forecastTableView.frame = view.bounds
        
    }
    
}
extension ForecastTabViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightForHeader
    }
    
}
extension ForecastTabViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        for element in forecastPresenter.forecastData {
            let sectionTitle = section == 0 ? "Today" : element.dt.getFormattedDate(format: "EEEE", value: section)
            return sectionTitle
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return forecastPresenter.forecastData.count / 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let forecastCell = tableView.dequeueReusableCell(withIdentifier: ForecastTableViewCell.cellIdentifier, for: indexPath) as? ForecastTableViewCell else {return UITableViewCell()}
        let model = forecastPresenter.forecastData[indexPath.row]
        forecastCell.populateTableViewCells(with: model)
        return forecastCell
    }
    
}
extension ForecastTabViewController: ForecastTabViewProtocol {
    func update() {
        DispatchQueue.main.async {
            self.forecastTableView.reloadData()
        }
    }
    
    
}
