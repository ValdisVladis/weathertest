//
//  MainTabViewController.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//

import UIKit

class MainTabViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabController()
        
        // Do any additional setup after loading the view.
    }
    
    func setupTabController() {
        
        let todayViewController = createNavigationController(vc: TodayViewController(), selectedStateImage: UIImage(systemName: "sun.max") ?? UIImage(), unselectedStateImage: UIImage(systemName: "sun.max") ?? UIImage())
        todayViewController.showActivityIndicator()

        let forecastTabViewController = createNavigationController(vc: ForecastTabViewController(), selectedStateImage: UIImage(systemName: "thermometer.sun") ?? UIImage(), unselectedStateImage: UIImage(systemName: "thermometer.sun") ?? UIImage())
        
        viewControllers?.first?.title = "Today"
        viewControllers?.last?.title = "Forecast"
        viewControllers = [todayViewController, forecastTabViewController]
        
        guard let items = tabBar.items else {return}
        items.map { component  in
            component.imageInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            
        }
        
    }
}
