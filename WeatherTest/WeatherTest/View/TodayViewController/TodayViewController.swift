//
//  TodayViewController.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//
import TinyConstraints
import UIKit

class TodayViewController: UIViewController  {
    //    MARK: - Constants
    
    let urlForDataFetching = "\(URLConstants.baseURL)/data/2.5/forecast?lat=53.893009&lon=27.567444&units=metric&appid=\(URLConstants.apiKey)"
    //MARK: - UIComponents
    var secondSeparatorView = UIView()
    var thirdSeparatorView = UIView()
    var pressureLabel = UILabel()
    var temperatureLabel = UILabel()
    var weatherDescriptionLabel = UILabel()
    var timezoneLabel = UILabel()
    let windSpeedLabel = UILabel()
    let rainLabel = UILabel()
    let weatherIcon = UIImageView()
    var pressureIcon = UIImageView()
    var windIcon = UIImageView()
    var rainIcon = UIImageView()
    var shareButton = UIButton()
    
    // MARK: - Variables
    private var temperature: String = ""
    private var clouds: String = ""
    private var windSpeed: String = ""
    var presenter = WeatherPresenter(with: DataFetcherService())
    
    // MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(r: 0, g: 173, b: 255)
        title = "Today"
        
        presenter.fetchWeatherData()
        setupUI()
        presenter.setWeatherViewDelegate(weatherViewDelegate: self)
        [weatherIcon, timezoneLabel, temperatureLabel, weatherDescriptionLabel, secondSeparatorView, thirdSeparatorView, pressureIcon, pressureLabel, windIcon, windSpeedLabel, rainIcon, rainLabel, shareButton].forEach { (view.addSubview($0))}
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkInternetConnection()
        presenter.weatherDataHandler = {  data in
            self.checkDataAvailability(data)
            data.list.map { [weak self ] (dataModel)  in
                guard let self = self else {return}
                self.temperature = "The current temp is \(Int(dataModel.main.temp)) °C"
                self.clouds = "the cloud percentage is \(Int(dataModel.clouds.all ?? Int())) %"
                self.windSpeed = "The current wind speed is \(Int(dataModel.wind.speed))"
            }
        }
        
        presenter.locationNameHandler = { [weak self] locationName in
            guard let self = self else {return}
            self.timezoneLabel.text = locationName
        }
        
    }
    
    private func checkDataAvailability(_ data: Any) {
        if data != nil {
            self.removeActivityIndicator()
        }
    }
    
    private func checkInternetConnection() {
        if !NetworkMonitor.shared.isConnected {
            UIAlertController.showNetworkUnavailability(on: self,
                                                        title: "Mobile Data Is Turned Off",
                                                        message: "Turn on mobile data or use Wi-Fi to access data",
                                                        preferredStyle: .alert)
        }
    }
    
    @objc func shareWeatherWithDifferentApplications() {
        ActivityController.shared.shareActivityWithDifferentApps(showingOn: self, activityItems: [temperature, clouds, windSpeed], applicationActivities: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: - AutoLayout
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupConstraints()
       
    }
}
