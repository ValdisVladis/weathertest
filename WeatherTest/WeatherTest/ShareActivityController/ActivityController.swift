//
//  ActivityController.swift
//  WeatherTest
//
//  Created by Владислав on 6/7/21.
//

import Foundation
import UIKit

class ActivityController {
    
    static let shared = ActivityController()
    
    func shareActivityWithDifferentApps(showingOn controller: UIViewController,
                                        activityItems: [Any],
                                        applicationActivities: [UIActivity]?) {
        let activityController = UIActivityViewController(activityItems: activityItems,
                                                          applicationActivities: applicationActivities)
        DispatchQueue.main.async {
            controller.present(activityController, animated: true, completion: nil)
        }
    }
}
