//
//  ForecastTableViewCell.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//

import UIKit
//MARK: - TableViewDelegate

protocol TableViewCellDelegate {
    func populateTableViewCells(with object: DataModel?)
}

class ForecastTableViewCell: UITableViewCell {
    
    //MARK: - Constants
    
    static let cellIdentifier = "cellIdentifier"
    var temperatureLabel = UILabel()
    // MARK: - TableViewCell configuration
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.rgb(r: 100, g: 217, b: 217)
        temperatureLabel = UILabel.createReusableLabel(font: UIFont(name: "Helvetica", size: 50) ?? UIFont(), textColor: .blue, alignment: .center)
        contentView.addSubview(temperatureLabel)
        temperatureLabel.activateConstraintsOnView(top: contentView.topAnchor,
                                                   leading: nil,
                                                   bottom: contentView.bottomAnchor,
                                                   trailing: contentView.trailingAnchor,
                                                   padding: .init(top: 5, left: 0, bottom: 0, right: -30),
                                                   size: .init(width: 150, height: 70))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
// MARK: - TableViewCell extension
extension ForecastTableViewCell: TableViewCellDelegate {
    func populateTableViewCells(with object: DataModel?) {
        object?.weather.map({ element in
            imageView?.image = getWeatherImage(name: element.icon)
            detailTextLabel?.text = element.main
            
        })
        textLabel?.text = object?.dt.getFormattedTime(format: "HH: mm")
        temperatureLabel.text = "\(Int(object?.main.temp ?? Double())) °C"
    }
}
