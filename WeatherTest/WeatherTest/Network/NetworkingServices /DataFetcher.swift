//
//  DataFetcher.swift
//  WeatherTest
//
//  Created by Владислав on 6/2/21.
//

import Foundation
import UIKit

protocol DataFetching {
    func fetchGenericJSONData<T: Decodable>(urlString: String, completion: @escaping (T?)-> Void)
}

class DataFectcher: DataFetching {
    
    var networking: Networking
    init(networking: Networking = APICaller()) {
        self.networking = networking
    }

    func fetchGenericJSONData<T: Decodable>(urlString: String, completion: @escaping (T?)-> Void) {
        networking.dataRequest(urlString: urlString) { (data, error) in
            
            if let error = error {
                print("Error received requesting data: \(error.localizedDescription)")
                completion(nil)
            }
            let decoded = self.decodeJSON(type: T.self, from: data)
            completion(decoded)
           
        }
    }
    func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = from else {return nil}
        do {
            
            let objects = try decoder.decode(type.self, from: data)
            return objects
        }catch let jsonError {
            print("Failed to decode JSON", jsonError)
            return nil
        }
    }
}
