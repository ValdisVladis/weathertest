//
//  DataFetcherService.swift
//  WeatherTest
//
//  Created by Владислав on 6/4/21.
//

import Foundation
import UIKit

class DataFetcherService {
    var networkDataFetcher: DataFectcher!
    
    init(networkDataFetcher: DataFectcher = DataFectcher()) {
        self.networkDataFetcher = networkDataFetcher
    }
    
    func requestData(urlString: String, completion: @escaping (JSONModel?) -> Void) {
        networkDataFetcher.fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
}
